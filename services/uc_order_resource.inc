<?php

function _uc_order_resource() {
  return array(
    'uc_order' => array(
      'retrieve' => array(
        'file' => array('type' => 'inc', 'module' => 'uc_services', 'name' => 'services/uc_order_resource'),
        'access callback file' => array('type' => 'inc', 'module' => 'uc_services', 'name' => 'services/uc_order_resource'),
        'callback' => '_uc_order_retrieve',
        'access callback' => 'uc_services_access_services',
        'access arguments' => array('uc_order', 'retrieve'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'order_id',
            'optional' => FALSE,
            'source' => array('path' => 0),
            'type' => 'int',
            'description' => 'The order ID to get.',
          ),
        ),
      ),
      'index' => array(
        'file' => array('type' => 'inc', 'module' => 'uc_services', 'name' => 'services/uc_order_resource'),
        'access callback file' => array('type' => 'inc', 'module' => 'uc_services', 'name' => 'services/uc_order_resource'),
        'callback' => '_uc_order_index',
        'access callback' => 'uc_services_access_services',
        'access arguments' => array('uc_order', 'index'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'page',
            'optional' => TRUE,
            'type' => 'int',
            'description' => 'The zero-based index of the page to get, defaults to 0.',
            'default value' => 0,
            'source' => array('param' => 'page'),
          ),
          array(
            'name' => 'fields',
            'optional' => TRUE,
            'type' => 'string',
            'description' => 'The fields to get.',
            'default value' => '*',
            'source' => array('param' => 'fields'),
          ),
          array(
            'name' => 'parameters',
            'optional' => TRUE,
            'type' => 'array',
            'description' => 'Parameters array',
            'default value' => array(),
            'source' => array('param' => 'parameters'),
          ),
          array(
            'name' => 'pagesize',
            'optional' => TRUE,
            'type' => 'int',
            'description' => 'Number of records to get per page.',
            'default value' => variable_get('services_uc_order_index_page_size', 20),
            'source' => array('param' => 'pagesize'),
          ),
        ),
      ),
      'create' => array(
        'help' => 'Creates an order',
        'file' => array('type' => 'inc', 'module' => 'uc_services', 'name' => 'services/uc_order_resource'),
        'callback' => '_uc_order_create',
        'access callback' => 'uc_services_access_services',
        'access arguments' => array('uc_order', 'create'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'order',
            'type' => 'array',
            'description' => 'The order object',
            'source' => 'data',
            'optional' => FALSE,
          ),
        ),
      ),
    ),
  );
}

/**
 * Get one order.
 */
function _uc_order_retrieve($order_id) {
  $order = uc_order_load($order_id);
  if ($order) {
    $order->uri = services_resource_uri(array('uc_order', $order_id));
    return $order;
  }
  else {
    return services_error('Order ' . $order_id . ' not found', 404);
  }
}

/**
 * Get many orders.
 */
function _uc_order_index($page, $fields, $parameters, $page_size) {
  $out = array();

  $result = services_resource_build_index_query('uc_orders', 'created', $page, $fields, $parameters, 'uc_o', 'order_id', $page_size, 'uc_order');
  while ($row = db_fetch_object($result)) {
    $out[] = $row;
  }

  return services_resource_build_index_list($out, 'uc_orders', 'order_id');
}

function _uc_order_create($data) {
  if (!$data['uid']) {
    services_error('User not found.');
  }

  if (!$data['date']) {
    services_error('Missing parameter "date".');
  }

  if (!$data['sku']) {
    services_error('Missing parameter "sku".');
  }

  if (!is_array($data['sku'])) {
    $data['sku'] = explode(',', $data['sku']);
  }

  module_load_include('inc', 'uc_order', 'uc_order.admin');

  $order = uc_order_new($data['uid']);
  $order->uid = $data['uid'];
  $order->created = $data['date'];

  foreach ($data['sku'] as $sku) {
    $nid = db_result(db_query("select nid from {uc_products} where model = '%s'", $sku));
    if (!$nid) {
      services_error("Product with sku $sku not found.");
    }
    $product = node_load($nid);
    uc_order_product_save($order->order_id, $product);
  }

  foreach ($data['nid'] as $nid) {
    $product = node_load($nid);
    if (!$product) {
      services_error("Product with nid $nid not found.");
    }
    uc_order_product_save($order->order_id, $product);
  }

  uc_order_save($order);
  return $order;
}
