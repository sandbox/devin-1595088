<?php

function _uc_cart_product_resource() {
  return array(
    'uc_cart_product' => array(
      'index' => array(
        'file' => array('type' => 'inc', 'module' => 'uc_services', 'name' => 'services/uc_cart_product_resource'),
        'access callback file' => array('type' => 'inc', 'module' => 'uc_services', 'name' => 'services/uc_cart_product_resource'),
        'callback' => '_uc_cart_product_index',
        'access callback' => 'uc_services_access_services',
        'access arguments' => array('uc_cart_product', 'index'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'page',
            'optional' => TRUE,
            'type' => 'int',
            'description' => 'The zero-based index of the page to get, defaults to 0.',
            'default value' => 0,
            'source' => array('param' => 'page'),
          ),
          array(
            'name' => 'fields',
            'optional' => TRUE,
            'type' => 'string',
            'description' => 'The fields to get.',
            'default value' => '*',
            'source' => array('param' => 'fields'),
          ),
          array(
            'name' => 'parameters',
            'optional' => TRUE,
            'type' => 'array',
            'description' => 'Parameters array',
            'default value' => array(),
            'source' => array('param' => 'parameters'),
          ),
          array(
            'name' => 'pagesize',
            'optional' => TRUE,
            'type' => 'int',
            'description' => 'Number of records to get per page.',
            'default value' => variable_get('services_uc_cart_product_index_page_size', 20),
            'source' => array('param' => 'pagesize'),
          ),
        ),
      ),
    ),
  );
}

/**
 * Get many cart products.
 */
function _uc_cart_product_index($page, $fields, $parameters, $page_size) {
  $out = array();

  if ($parameters['uid']) {
    $parameters['cart_id'] = $parameters['uid'];
    unset($parameters['uid']);
  }

  $result = services_resource_build_index_query('uc_cart_products', 'cart_item_id', $page, $fields, $parameters, 'uc_o_p', 'order_product_id', $page_size, 'uc_cart_product');
  while ($row = db_fetch_object($result)) {
    $row->data = unserialize($row->data);
    $row->uid = $row->cart_id;
    unset($row->cart_id);
    $out[] = $row;
  }

  return services_resource_build_index_list($out, 'uc_cart_product', 'cart_item_id');
}
