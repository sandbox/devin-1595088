<?php

function _uc_order_product_resource() {
  return array(
    'uc_order_product' => array(
      'retrieve' => array(
        'file' => array('type' => 'inc', 'module' => 'uc_services', 'name' => 'services/uc_order_product_resource'),
        'access callback file' => array('type' => 'inc', 'module' => 'uc_services', 'name' => 'services/uc_order_product_resource'),
        'callback' => '_uc_order_product_retrieve',
        'access callback' => 'uc_services_access_services',
        'access arguments' => array('uc_order_product', 'retrieve'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'order_product_id',
            'optional' => FALSE,
            'source' => array('path' => 0),
            'type' => 'int',
            'description' => 'The order product ID to get.',
          ),
        ),
      ),
      'index' => array(
        'file' => array('type' => 'inc', 'module' => 'uc_services', 'name' => 'services/uc_order_product_resource'),
        'access callback file' => array('type' => 'inc', 'module' => 'uc_services', 'name' => 'services/uc_order_product_resource'),
        'callback' => '_uc_order_product_index',
        'access callback' => 'uc_services_access_services',
        'access arguments' => array('uc_order_product', 'index'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'page',
            'optional' => TRUE,
            'type' => 'int',
            'description' => 'The zero-based index of the page to get, defaults to 0.',
            'default value' => 0,
            'source' => array('param' => 'page'),
          ),
          array(
            'name' => 'fields',
            'optional' => TRUE,
            'type' => 'string',
            'description' => 'The fields to get.',
            'default value' => '*',
            'source' => array('param' => 'fields'),
          ),
          array(
            'name' => 'parameters',
            'optional' => TRUE,
            'type' => 'array',
            'description' => 'Parameters array',
            'default value' => array(),
            'source' => array('param' => 'parameters'),
          ),
          array(
            'name' => 'pagesize',
            'optional' => TRUE,
            'type' => 'int',
            'description' => 'Number of records to get per page.',
            'default value' => variable_get('services_uc_order_product_index_page_size', 20),
            'source' => array('param' => 'pagesize'),
          ),
        ),
      ),
    ),
  );
}

/**
 * Get one order product.
 */
function _uc_order_product_retrieve($order_product_id) {
  $sql = "select * from {uc_order_products} where order_product_id = %d";
  $result = db_query($sql, $order_product_id);
  $uc_order_product = db_fetch_object($result);
  if ($uc_order_product) {
    $uc_order_product->uri = services_resource_uri(array('uc_order_product', $order_product_id));
    return $uc_order_product;
  }
  else {
    return services_error('Order product ' . $order_product_id . ' not found', 404);
  }
}

/**
 * Get many order products.
 */
function _uc_order_product_index($page, $fields, $parameters, $page_size) {
  $out = array();

  $result = services_resource_build_index_query('uc_order_products', 'order_product_id', $page, $fields, $parameters, 'uc_o_p', 'order_product_id', $page_size, 'uc_order_product');
  while ($row = db_fetch_object($result)) {
    $out[] = $row;
  }

  return services_resource_build_index_list($out, 'uc_order_products', 'order_product_id');
}
